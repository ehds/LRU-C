#ifndef __LRUCACHE_H__
#define __LRUCACHE_H__

int LRUCacheCreate(int capacity, void **lruCache);

int LRUCacheDestory(void *lruCache);

int LRUcacheSet(void *lruCache, char key, char data);

int LRUCacheGet(void *lruCache, char key);

int LRUCachePrint(void *lruCache);

#endif // __LRUCACHE_H__