#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include "LRUCache.h"
#include "LRUCacheImpl.h"

static void freeList(LRUCacheS *cache);
static cacheEntryS* newCacheEntry(char key, char data){
    cacheEntryS* entry = NULL;
    if(NULL==(entry=malloc(sizeof(*entry)))){
        perror("malloc newCacheEntry");
        return NULL;
    }
    memset(entry,0,sizeof(*entry));
    entry->key = key;
    entry->data = data;
    return entry;
}
static void freeCacheEntry(cacheEntryS* entry){
    if(NULL==entry) return;
    free(entry);
}

//创建一个LRU缓存
int LRUCacheCreate(int capacity,void **lruCache){
    LRUCacheS* cache = NULL;
    if(NULL == (cache=malloc(sizeof(*cache)))){
        perror("malloc LRUCacheS");
        return -1;
    }
    memset(cache,0,sizeof(*cache));
    cache->cacheCapacity = capacity;
    cache->hashMap = malloc(sizeof(cacheEntryS)*capacity);
    if (NULL ==cache->hashMap){
        free(cache);
        perror("malloc");
        return -1;
    }
     memset(cache->hashMap, 0, sizeof(cacheEntryS)*capacity);
    *lruCache = cache;
    return 0;
}
int LRUCacheDestory(void *lruCache){

    LRUCacheS* cache = (LRUCacheS*)lruCache;
    if(NULL==lruCache) return 0;
    if(cache->hashMap)
        free(cache->hashMap);
    freeList(cache);
    free(cache);
    return 0;
}

static void removeFromList(LRUCacheS *cache,cacheEntryS *entry){
    //链表为空
    if(cache->lruListSize==0) return;
    if (entry==cache->lruListHead && entry==cache->lruListTail){
        cache->lruListHead = cache->lruListTail;
    }
}